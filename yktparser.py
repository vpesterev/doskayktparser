#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import urllib2
import json
import re
from time import time, ctime
import smtplib
from email.mime.text import MIMEText
from time import sleep
import os

base_dir = os.path.dirname(__file__)

host = "http://doska.ykt.ru"

def writeLog(info):
	f = open(os.path.join(base_dir, 'page.log'), 'a')
	f.write(ctime(time()) + '\n')
	f.write(info.encode('utf-8'))
	f.write('\n\n\n')
	f.close()
	
def getConf():
	f = open(os.path.join(base_dir, 'conf.json'), 'r')
	conf = f.read()
	f.close()
	return json.loads(conf)
	
def setConf(newConf):
	f = open(os.path.join(base_dir, 'conf.json'), 'w')
	f.write(json.dumps(newConf))
	f.close()

def postParse(post):
	soup = BeautifulSoup(str(post), "html.parser")
	title = ""
	try:
		title = soup.find("div", {"class": "d-post_title"}).text
	except AttributeError:
		title = "No title"
	description = ""
	try:
		description = soup.find("div", {"class": "d-post_desc"}).text
	except AttributeError:
		description = "No description"
	price = ""
	try:
		price = soup.find("div", {"class": "d-post_price"}).text
	except AttributeError:
		price = "No price"
	date = soup.find("div", {"class": "d-post_date"}).text
	phone = ""
	try:
		phone = soup.find("div", {"class": "d-post_phone"}).text
	except AttributeError:
		phone = "No phone"
	divlink = soup.find("a", {"class": "d-post_link"})
	link = host + divlink["href"]
		
	return title + "\n" + description + "\n" + price + "\n" + date + "\n" + phone + "\n" + link + "\n"
	
def sendMessage(content):
	confJson = getConf()
	msg = MIMEText(content, "plain", "utf-8")
	msg["Subject"] = "Пакет уведомлений YktParser"
	msg["From"] = confJson["account"]["username"]
	msg["To"] = confJson["account"]["username"]
	s = smtplib.SMTP(confJson["account"]["smtp_server"])
	s.starttls()
	s.login(confJson["account"]["username"], confJson["account"]["password"])
	s.sendmail(confJson["account"]["username"], confJson["emails"], msg.as_string())
	s.quit()

def pagesParse(page):
	confJson = getConf()
	last_post_id = confJson['last_post_id']
	soup = BeautifulSoup(page, "html.parser")
	posts = soup.findAll("div", {
									"class": "d-post"
								})
	maxim = last_post_id
	message = ""
	for post in posts:
		m = re.findall('[0-9]+', post["id"])
		post_id = m[0]
		if int(post_id) > int(last_post_id):
			if int(post_id) > int(maxim):
				maxim = post_id
			postContent = postParse(post)
			writeLog(postContent)
			message += "\n\n\n" + postContent
	if message is not "":
		sendMessage(message)
		confJson['last_post_id'] = str(maxim)
		setConf(confJson)

try:
	writeLog('parse is runned...')
	page = urllib2.urlopen(host + "/posts?cid=134&sid=205&rid=232").read()
	pagesParse(page)
except urllib2.URLError:
	writeLog('Проблемы соединения с хостом ' + host + '. Возможно вы не подключены к сети интернет.')
